output "aks_id" {
  value = azurerm_kubernetes_cluster.TF_aks.id
}

output "aks_fqdn" {
  value = azurerm_kubernetes_cluster.TF_aks.fqdn
}

output "cluster_username" {
  value = azurerm_kubernetes_cluster.TF_aks.kube_config.0.username
  sensitive = true
}

output "cluster_password" {
  value     = azurerm_kubernetes_cluster.TF_aks.kube_config.0.password
  sensitive = true
}

# Create kubeconfig file of the created cluster 

output "kube_config" {
  value     = azurerm_kubernetes_cluster.TF_aks.kube_config_raw
  sensitive = true
}
