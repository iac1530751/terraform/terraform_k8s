variable "resource_group_name" {
  type        = string
  description = "RG name in Azure"
  default     = "sbdp-test-rg"
}

variable "resource_location" {
  type        = string
  description = "RG location in Azure"
  default     = "West Europe"
}

variable "cluster_name" {
  type        = string
  description = "AKS cluster name in Azure"
  default     = "sbdp-test-aks"
}

variable "system_node_count" {
  type        = number
  description = "Worker Node count for AKS"
  default     = 1
}

variable "kubernetes_version" {
  type        = string
  description = "Kubernetes version"
}

variable "vm_size" {
  type        = string
  description = "VM size for worker node"
  default     = "Standard_D2_v2"
}

variable "username" {
  type        = string
  description = "The admin username for the new cluster."
  default     = "azureadmin"
}