

# Below are the resources that will be created - 

# Create a resource group
resource "azurerm_resource_group" "TF_rg" {
  name     = var.resource_group_name
  location = var.resource_location

  tags = {
    environment = "test"
  }

}

# Create AKS cluster 

resource "azurerm_kubernetes_cluster" "TF_aks" {
  name                = var.cluster_name
  kubernetes_version  = var.kubernetes_version
  location            = azurerm_resource_group.TF_rg.location
  resource_group_name = azurerm_resource_group.TF_rg.name
  dns_prefix          = var.cluster_name

  default_node_pool {
    name       = "default"
    node_count = var.system_node_count
    vm_size    = var.vm_size
  }

  identity {
    type = "SystemAssigned"
  }

  linux_profile {
    admin_username = var.username

    ssh_key {
      key_data = jsondecode(azapi_resource_action.ssh_public_key_gen.output).publicKey
    }
  }

  network_profile {
    network_plugin    = "kubenet"
    load_balancer_sku = "standard"
  }

  tags = {
    Environment = "Test"
  }
}