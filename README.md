# Terraform K8s



Create Infra using TF 
```
# terraform init 
# terraform validate 

# terraform plan -out main.tfplan
# terraform apply main.tfplan 
```

```
# terraform output kube_config 
```

Destroy the Infra created using TF 
```
# terraform plan -destroy 
# terraform apply -destory 
```

Important Links - 
[Azure+K8s official](https://learn.microsoft.com/en-us/azure/aks/learn/quick-kubernetes-deploy-terraform?tabs=bash)